﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class ShieldPowerup : MonoBehaviour
{
    Transform player = null;
    [SerializeField] private int pointsValue = 100;
    // Start is called before the first frame update
    void Start()
    {
        player = transform.parent;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            player.GetComponent<PolygonCollider2D>().enabled = true;
            transform.GetComponent<CircleCollider2D>().enabled = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            player.GetComponent<PlayerController>().powerupsText.text = "Powerups: None";
        }
        if (collision.gameObject.tag == "ShieldPowerUp")
        {
            player.GetComponent<PolygonCollider2D>().enabled = false;
            player.GetChild(1).GetComponent<CircleCollider2D>().enabled = true;
            player.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            player.GetComponent<PlayerController>().UpdateScore(pointsValue);
            Destroy(collision.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
