﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuHandler : MonoBehaviour
{
	private CanvasGroup _canvasGroup;

	GameObject PauseMenuSound = null;
	// Use this for initialization
	void Start()
	{
		foreach (Transform child in transform)
		{
			if (child.tag == "PauseMenuSound")
				PauseMenuSound = child.gameObject;
		}
		_canvasGroup = GetComponent<CanvasGroup>();
		Time.timeScale = 1;
		pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
		hideObjects = GameObject.FindGameObjectsWithTag("HideOnPause");
		hidePaused();
	}

	// Update is called once per frame
	void Update()
	{
		UnityEngine.Debug.Log("Hi");
		//uses the Escape button to pause and unpause the game
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (Time.timeScale == 1)
			{

				Time.timeScale = 0;
				showPaused();
			}
			else if (Time.timeScale == 0)
			{
				Time.timeScale = 1;
				hidePaused();
			}
		}
	}

	//Closes the game
	GameObject[] pauseObjects;
	GameObject[] hideObjects;
	public void doExitGame()
	{
		Application.Quit();
	}

	//Reloads the Level
	public void Reload()
	{
		Application.LoadLevel(Application.loadedLevel);
	}

	//controls the pausing of the scene
	public void pauseControl()
	{
		if (Time.timeScale == 1)
		{
			Time.timeScale = 0;
			showPaused();
		}
		else if (Time.timeScale == 0)
		{
			Time.timeScale = 1;
			hidePaused();
		}
	}

	public void FadeIn()
	{
		StartCoroutine(FadeInCoroutine());
	}

	IEnumerator FadeInCoroutine()
	{
		_canvasGroup.alpha = 0f;
		LeanTween.alphaCanvas(_canvasGroup, 1f, 1f).setEase(LeanTweenType.linear);
		yield return new WaitForSeconds(1);

	}
	public void FadeOut()
	{
		StartCoroutine(FadeOutCoroutine());
	}

	IEnumerator FadeOutCoroutine()
	{
		LeanTween.alphaCanvas(_canvasGroup, 0f, 1f).setEase(LeanTweenType.linear);
		yield return new WaitForSeconds(1);
		gameObject.SetActive(false);
	}


	//shows objects with ShowOnPause tag
	public void showPaused()
	{
		foreach (GameObject g in pauseObjects)
		{
			g.SetActive(true);
		}

		_canvasGroup.GetComponent<Canvas>().enabled = true;
		PauseMenuSound.GetComponent<AudioSource>().enabled = true;

		foreach (GameObject g in hideObjects)
		{
			g.SetActive(false);
		}
	}

	//hides objects with ShowOnPause tag
	public void hidePaused()
	{
		//foreach (GameObject g in pauseObjects)
		//{
		//	g.SetActive(false);
		//}
		_canvasGroup.GetComponent<Canvas>().enabled = false;
		PauseMenuSound.GetComponent<AudioSource>().enabled = false;

		foreach (GameObject g in hideObjects)
		{
			g.SetActive(true);
		}
	}


	//loads inputted level
	public void LoadLevel(string level)
	{
		Application.LoadLevel(level);
	}
}
