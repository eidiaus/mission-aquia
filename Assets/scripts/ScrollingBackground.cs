﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float scroll_speed = 0.1f;

    private MeshRenderer mesh_renderer;

    private Vector2 saved_offset;
    // Start is called before the first frame update
    void Start()
    {
        mesh_renderer = GetComponent<MeshRenderer>();

        saved_offset = mesh_renderer.sharedMaterial.GetTextureOffset("_MainTex");
    }

    // Update is called once per frame
    void Update()
    {
        float x = Time.time * scroll_speed;

        Vector2 offset = new Vector2(x, 0);

        mesh_renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);

    }

    void OnDisable()
    {
        mesh_renderer.sharedMaterial.SetTextureOffset("_MainTex", saved_offset);
    }
}
