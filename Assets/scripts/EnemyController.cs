﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private GameObject[] powerUps = new GameObject[1] { null };
    [SerializeField] private float spawnChance = 0.1f;
    [SerializeField] private int pointsValue = 10;
    private float moveSpeed = 1.0f;
    private Rigidbody2D rb;
    private Transform player;
    private Vector2 movement;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        moveEnemy(); 
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameController.UpdateHealth(-1);
        }

        if (collision.gameObject.tag == "Bullet")
        {
            if (UnityEngine.Random.value > (1-spawnChance))
            {
                Instantiate(powerUps[UnityEngine.Random.Range(0, powerUps.Count())], transform.position, transform.rotation);
            }
            player.GetComponent<PlayerController>().UpdateScore(pointsValue);
        }
    }

    void moveEnemy()
    {

        Vector3 direction = player.position - transform.position;
        float angle = (Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg) - 180f;
        rb.rotation = angle;
        direction.Normalize();
        movement = direction;
        moveSpeed = 0.3f / gameObject.transform.localScale.x;
        transform.Translate(direction * moveSpeed * Time.deltaTime, Space.World);
    }
}

