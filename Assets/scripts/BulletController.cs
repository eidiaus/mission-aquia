﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private float speed = 10f;
    private Transform player;
    [SerializeField] private GameObject _explosionPrefab = null;

    //y -7, 7, x -15, 15

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            GameObject explosion = Instantiate(_explosionPrefab, transform.position, transform.rotation);
            explosion.transform.localScale = Vector3.Scale(collision.gameObject.transform.localScale, new Vector3(4, 4, 0));
            Destroy(gameObject);
            Destroy(collision.gameObject);
            Destroy(gameObject);

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, player.eulerAngles.z);

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
        if ((transform.position.x > 15)||(transform.position.x < -15)||(transform.position.y > 7)||(transform.position.y < -7))
        {
            Destroy(transform.gameObject);
        }
    }
}
