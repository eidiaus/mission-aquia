﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using UnityEngine.UI; 

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private GameObject bulletPrefab = null;
    [SerializeField] private float fireDelayTime = 1.0f;
    private float fireTime = 0.0f;
    public Text scoreText;
    public Text powerupsText;
    private float angleValue;
    private float angleValue2;

    public void UpdateScore(int value)
    {
        int score= 0;
        if (int.TryParse(scoreText.text, out score))
        {
            score += value;
            scoreText.text = score.ToString();
        }
        else
        {
            scoreText.text = value.ToString();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        movePlayer();
        playerFire();
        faceMouse();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "ShieldPowerUp")
        {
            transform.GetComponent<PolygonCollider2D>().enabled = false;
            transform.GetChild(1).GetComponent<CircleCollider2D>().enabled = true;
            transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            //powerupsText.text = powerupsText.text + " Shield";
            powerupsText.text = "Powerups: Shield";
            Destroy(collision.gameObject);
        }
    }


    void faceMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        transform.up = direction;
    }

    void movePlayer()
    {

        float upDownInput = Input.GetAxis("Vertical");

        Vector2 verticalMoveVector = Vector2.up * upDownInput;
        Vector2 moveVector = verticalMoveVector * speed * Time.deltaTime;
        
        if (Input.GetMouseButton(1))
        {
            var pos = Input.mousePosition;
            pos.z = transform.position.z - Camera.main.transform.position.z;
            pos = Camera.main.ScreenToWorldPoint(pos);
            transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(moveVector);
        }

    }

    void playerFire()
    {
        fireTime -= Time.deltaTime;

        if (Input.GetButton("Fire1") && fireTime <= 0.0f)
        {
            if (Mathf.Sin(transform.eulerAngles.z - 90) == 0)
            {
                angleValue = 1;
            }
            else
            {
                angleValue = Mathf.Sin(transform.eulerAngles.z - 90);
                if (angleValue < 0)
                {
                    angleValue = Mathf.Sin(transform.eulerAngles.z - 90) * -1;
                }
            }
            if ((transform.eulerAngles.z >= 0) && (transform.eulerAngles.z <= 180))
            {
                angleValue = angleValue * -1;
            }

            angleValue2 = Mathf.Sin(transform.eulerAngles.z);
            GameObject bullet = Instantiate(bulletPrefab, transform.position + new Vector3(0.3f*angleValue2, -0.3f*angleValue, 0), Quaternion.identity);
            
            fireTime = fireDelayTime;
        }
    }
}
