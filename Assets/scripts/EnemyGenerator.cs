﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField] private GameObject EnemyPrefab;
    [SerializeField] private float EnemySpawnTime = 1.0f;
    [SerializeField] private float radius = 5f;
    private float countdown = 0.0f;

    System.Random rndGen;


    void Start()
    {
        rndGen = new System.Random();
    }

    void Update()
    {
        countdown -= Time.deltaTime;

        if (countdown <= 0.0f)
        {
            generateEnemy();
            countdown = EnemySpawnTime;
        }
    }

    void generateEnemy()
    {
        float angle = UnityEngine.Random.Range(0, (float)Math.PI * 2);
        Vector2 position = new Vector2(Mathf.Sin(angle)*1.5f*radius, Mathf.Cos(angle)*radius);
        GameObject enemy = Instantiate(EnemyPrefab, new Vector2(position.x, position.y), Quaternion.identity);

        double scale = GetRandomNumber(0.125,0.25);
        enemy.transform.localScale = new Vector3((float)scale, (float)scale, 0);
    }

    public double GetRandomNumber(double minimum, double maximum)
    {
        System.Random random = new System.Random();
        return random.NextDouble() * (maximum - minimum) + minimum;
    }
}